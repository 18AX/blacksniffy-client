﻿using System;
using System.Runtime.CompilerServices;

namespace Json_Interpreter
{
    public class JSON_ChooseDevice : JSON
    {
        public int numDevice;
        
        public JSON_ChooseDevice(string command, string numDevice)
        {
            _command = command;
            int a;
            if (Int32.TryParse(numDevice, out a))
            {
                this.numDevice = Int32.Parse(numDevice);
            }
        }
    }
}