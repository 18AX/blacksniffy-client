﻿using System.Collections.Generic;
     
namespace Json_Interpreter
{
    public class JSON_NetworkHost : JSON
    {
             
        public Dictionary<string, string> _KeyValue { get; set; }
             
             
        public JSON_NetworkHost(string command, Dictionary<string, string> keyValue)
        {
            _command = command;
            _KeyValue = keyValue;
        }
     
        public JSON_NetworkHost()
        {
        }
    }
}