﻿
 
namespace Json_Interpreter
{
    public class JSON_NetworkScanner : JSON
    {
       
        public string _IP1 { get; set; }
       
        public string _IP2 { get; set; }
       
       
        public JSON_NetworkScanner(string command, string IP1, string IP2, string device)
        {
            _command = command;
            _IP1 = IP1;
            _IP2 = IP2;
        }
    }
}