namespace Json_Interpreter
{
    public enum Command
    {
        Error,
        NetworkScanner,
        CutConnection,
        ManInTheMiddle,
        AskForDevices,
        ChooseDevice,
        StartMitm,
        StopMitm
    }


}