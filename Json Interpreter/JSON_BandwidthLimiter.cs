﻿namespace Json_Interpreter
{
    public class JSON_BandwidthLimiter : JSON
    {
        public string value;
        
        public JSON_BandwidthLimiter(string command, string value)
        {
            _command = command;
            this.value = value;
        }
    }
}