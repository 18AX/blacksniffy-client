﻿namespace Json_Interpreter
{
    public class JSON_ManInTheMiddle : JSON
    {
        public string MacRooter;
        public string IpRooter;
        public string MacVictim;
        public string IpVictim;

        public JSON_ManInTheMiddle(string command, string macRooter, string ipRooter, string macVictim, string ipVictim)
        {
            _command = command;
            MacRooter = macRooter;
            IpRooter = ipRooter;
            MacVictim = macVictim;
            IpVictim = ipVictim;
        }
    }
}