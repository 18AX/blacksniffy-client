using System.Net;
using System.Net.NetworkInformation;
using PacketDotNet;
using SharpPcap;

namespace BlackSniffyCore.packet
{
    public class BasicArpPacket : BasicPacket
    {
        
        /**
         * On créé un paquet ARP utilse dans l'ARp spoofing (pour plus d'info cf wikipedia)
         */
        public BasicArpPacket(ICaptureDevice device) : base(device)
        {
        }

        public override void CreateAndSend(PhysicalAddress srcMac, PhysicalAddress dstMac, IPAddress srcIp, IPAddress dstIp)
        {
            ArpPacket arpPacket = new ArpPacket(ArpOperation.Request,
                dstMac, 
                dstIp, 
                srcMac, 
                srcIp);
            
            SendPacket(arpPacket, srcMac, dstMac, EthernetType.Arp);
        }
    }
}