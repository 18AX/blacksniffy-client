using System;
using System.Net;
using System.Net.NetworkInformation;
using PacketDotNet;
using SharpPcap;

namespace BlackSniffyCore.packet
{
    public abstract class BasicPacket : IDisposable
    {

        private ICaptureDevice _captureDevice;

        public BasicPacket(ICaptureDevice device)
        {
            _captureDevice = device;
        }

        public abstract void CreateAndSend(PhysicalAddress srcMac, PhysicalAddress dstMac, IPAddress srcIp, IPAddress dstIp);
        


        /**
         * Tous les paquets s'envoie de la même manière par la trame ethernet.
         */
        protected void SendPacket(Packet packet, PhysicalAddress srcAddress, PhysicalAddress dstAddress, EthernetType ethernetPacketType)
        {
            EthernetPacket ethernetPacket = new EthernetPacket(srcAddress, dstAddress, ethernetPacketType);
            ethernetPacket.PayloadPacket = packet;

            try
            {

                _captureDevice.SendPacket(ethernetPacket.Bytes);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error when sending packet : " + e);
            }
        }

        public void Dispose()
        {
            _captureDevice.Close();
        }
    }
}