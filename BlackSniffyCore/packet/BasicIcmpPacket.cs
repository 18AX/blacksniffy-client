using System.Net;
using System.Net.NetworkInformation;
using PacketDotNet;
using PacketDotNet.Utils;
using SharpPcap;

namespace BlackSniffyCore.packet
{
    public class BasicIcmpPacket : BasicPacket
    {
        public BasicIcmpPacket(ICaptureDevice device) : base(device)
        {
        }
        
        /**
         * Pas encore fait pour l'instant cela nous ai pas utile
         */

        public override void CreateAndSend(PhysicalAddress srcMac, PhysicalAddress dstMac, IPAddress srcIp, IPAddress dstIp)
        {
            
        }
    }
}