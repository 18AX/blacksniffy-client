using System.Net;
using System.Net.NetworkInformation;

namespace BlackSniffyCore
{
    public class NetworkHost
    {
        private readonly PhysicalAddress _physicalAddress;
        private readonly IPAddress _ipAddress;

        public NetworkHost(PhysicalAddress physicalAddress, IPAddress ipAddress)
        {
            _physicalAddress = physicalAddress;
            _ipAddress = ipAddress;
        }

        public PhysicalAddress GetPhysicalAddress()
        {
            return _physicalAddress;
        }

        public IPAddress GetIpAddress()
        {
            return _ipAddress;
        }

        public override bool Equals(object obj)
        {
            return (obj is NetworkHost) && ((NetworkHost) obj).GetPhysicalAddress().Equals(_physicalAddress) &&
                   ((NetworkHost) obj)._ipAddress.Equals(_ipAddress);
        }


        /**
         * Permet de comaprer les objets plus facilement. Logiquement un hashcode est unique.
         */
        public override int GetHashCode()
        {
            unchecked
            {
                return ((_physicalAddress != null ? _physicalAddress.GetHashCode() : 0) * 397) ^ (_ipAddress != null ? _ipAddress.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return "ip : " + _ipAddress.ToString() + " mac : " + _physicalAddress.ToString();
        }
    }
}