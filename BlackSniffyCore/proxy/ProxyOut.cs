using System;
using System.Collections.Generic;
using PacketDotNet;

namespace BlackSniffyCore.proxy
{
    public partial class Proxy
    {

        private Queue<Packet> _packetsForRooter; // On met des queue au cas ou il y a beaucoup de packets à traiter
        private Queue<Packet> _packetsForVictim;
        
        private object locker = new object(); // Le locket, evite à plusieurs thread d'acceder au attributs en même temps

        private bool Update()
        {
            /**
             * Comme dit au dessus le locket permet d'éviter que deux thread dequeu en même temps et donc de générer des erreurs
             */
            lock (locker)
            {
                if (_packetsForRooter.Count != 0)
                {
                    Packet packet = _packetsForRooter.Dequeue();
                    try
                    {
                        _captureDevice.SendPacket(packet.Bytes);
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine("Can't send the packet : " + e);
                    }
                }

                if (_packetsForVictim.Count != 0)
                {
                    Packet packet = _packetsForVictim.Dequeue();
                    try
                    {
                        _captureDevice.SendPacket(packet.Bytes);
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine("can't send the packet " + e);
                    }
                }

                
            }
            return true;
        }

        // On envoie le paquet sur l'internet. c'est à dire on les ajoutes au queue.
        public void SendPacket(Packet packet)
        {

            var eth = packet.Extract<EthernetPacket>();
            
            if (eth == null)
                return;
            
            if (eth.DestinationHardwareAddress.Equals(_rooter.GetPhysicalAddress()))
                _packetsForRooter.Enqueue(packet);
            
            if (eth.DestinationHardwareAddress.Equals(_victim.GetPhysicalAddress()))
                _packetsForVictim.Enqueue(packet);
        }

    }
}