using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BlackSniffyCore.proxy;
using PacketDotNet;
using PacketDotNet.Connections;
using PacketDotNet.Connections.Http;
using SharpPcap;

namespace BlackSniffyCore.analyser
{
    public class PacketAnalyser
    {
        public delegate void OnPacketTransitDelegate(Packet packet);

        public delegate void OnHttpWebsiteVisitedDelegate(string url);

        public delegate void OnHttpParameterDetectedDelegate(HTTPParameters parameters);
        
        private OnPacketTransitDelegate _onPacketTransitDelegate;

        private OnHttpWebsiteVisitedDelegate _onHttpWebsiteVisitedDelegate;

        private OnHttpParameterDetectedDelegate _onHttpParameterDetectedDelegate;

        private readonly HashSet<IPAddress> _blockedAddresses;

        private readonly TcpConnectionManager _connectionManager;

        private Task last;

        public PacketAnalyser()
        {
            _blockedAddresses = new HashSet<IPAddress>();
            _connectionManager = new TcpConnectionManager();
            _connectionManager.OnConnectionFound += HandleTcpSessionManager;
        }

        /**
         * Ce sont les funcs qui seront appeler cela permet de relier facilement à la partie utilisateurs.
         */
        public async void AddPacket(Proxy proxy, RawCapture rawCapture)
        {
            Action action = () =>
            {
                Packet packet = Packet.ParsePacket(rawCapture.LinkLayerType, rawCapture.Data);
                TcpPacket tcpPacket = packet.Extract<TcpPacket>();
                if (tcpPacket != null)
                {
                    ushort sourcePort = tcpPacket.SourcePort;
                    ushort destinationPort = tcpPacket.DestinationPort;
                    if (sourcePort == 80 || destinationPort == 80)
                    {
                        IPPacket ipPacket = packet.Extract<IPPacket>();
                        IPAddress ipAddress = ipPacket.SourceAddress;
                    
                        try
                        {
                            _connectionManager.ProcessPacket(rawCapture.Timeval, tcpPacket);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }

                        try
                        {
                            IPHostEntry dns = Dns.GetHostEntry(ipAddress);
                            string hostName = dns.HostName;

                            _onHttpWebsiteVisitedDelegate?.Invoke(hostName);
                        }
                        catch (Exception e)
                        {
                            //Console.WriteLine(e);
                        }
                    }

                    _onPacketTransitDelegate?.Invoke(packet);
                }
            };

            await Task.Run(action);

        }
        
        private void HandleTcpSessionManager(TcpConnection tcpConnection)
        {
            var httpSessionManager =
                new HttpSessionWatcher(tcpConnection, OnHttpRequestFound, OnHttpStatusFound, OnHttpWatcherError);
            
        }

        private void OnHttpRequestFound(HttpSessionWatcherRequestEventArgs requestEventArgs)
        {
            Console.WriteLine("Oulou :" + Encoding.UTF8.GetString(requestEventArgs.Request.Body));

            if (requestEventArgs.Request.Method == HttpRequest.Methods.Post)
            {
                string post = Encoding.UTF8.GetString(requestEventArgs.Request.Body);
                Console.WriteLine(post);
                HTTPParameters parameters = new HTTPParameters(post);
                
                Console.WriteLine("Analyser " + parameters);

                if (_onHttpParameterDetectedDelegate != null)
                    _onHttpParameterDetectedDelegate(parameters);
            }
        }
        
        private  void OnHttpStatusFound(HttpSessionWatcherStatusEventArgs e)
        {
            
            
            // only display compressed messages
            if((e.Status.ContentEncoding == HttpMessage.ContentEncodings.Deflate) ||
               (e.Status.ContentEncoding == HttpMessage.ContentEncodings.Gzip))
            {
                //Console.WriteLine(e.Status.ToString());
            }
        }
        
        private void OnHttpWatcherError(string errorString)
        {
            Console.WriteLine(errorString);
        }

        /*public OnIRCPacketDetectedDelegate GetIrcPacketDetected()
        {
            return _ircPacketDetectedDelegate;
        }

        public void SetIrcPacketDetected(OnIRCPacketDetectedDelegate p)
        {
            _ircPacketDetectedDelegate = p;
        }*/

        public OnPacketTransitDelegate OnPacketTransit
        {
            get => _onPacketTransitDelegate;
            set => _onPacketTransitDelegate = value;
        }

        public OnHttpWebsiteVisitedDelegate OnHttpWebsiteVisited
        {
            get => _onHttpWebsiteVisitedDelegate;
            set => _onHttpWebsiteVisitedDelegate = value;
        }

        public OnHttpParameterDetectedDelegate OnHttpParameterDetected
        {
            get => _onHttpParameterDetectedDelegate;
            set => _onHttpParameterDetectedDelegate = value;
        }

        public HashSet<IPAddress> BlockedAddresses => _blockedAddresses;

        public TcpConnectionManager ConnectionManager => _connectionManager;
    }
}