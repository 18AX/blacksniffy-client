using System.Collections.Generic;
using System.Text;

namespace BlackSniffyCore.analyser
{
    public class HTTPParameters
    {
        private Dictionary<string, string> _parameters;
        private string _post;
        
        public HTTPParameters(string post)
        {
            _post = post;
            Parse();
        }

        private void Parse()
        {
            _parameters = new Dictionary<string, string>();
            string[] tab = _post.Split('&');
            foreach (var parameter in tab)
            {
                string[] beauNom = parameter.Split('=');
                _parameters.Add(beauNom[0], beauNom[1]);
            }
        }

        public override string ToString()
        {
            StringBuilder res = new StringBuilder();
            foreach (var i in _parameters)
            {
                string toAdd = i.Key + " = " + i.Value;
                res.Append(toAdd + "\n");
            }

            return res.ToString();
        }
    }
}