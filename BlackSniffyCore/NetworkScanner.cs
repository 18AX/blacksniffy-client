using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using PacketDotNet;
using SharpPcap;

namespace BlackSniffyCore
{
    public class NetworkScanner
    {

        private ICaptureDevice _captureDevice;

        public NetworkScanner(ICaptureDevice captureDevice)
        {
            _captureDevice = captureDevice;
        }

        /**
         * On scan le réseau en envoyant des erquete ping à toues les ips dans cet range.
         */

        public NetworkHost[] ScanNetwork(IPAddress lowAddress, IPAddress upAddress)
        {
            Stopwatch stopwatch = new Stopwatch();
            
            stopwatch.Start();
            
            List<NetworkHost> networkHosts = new List<NetworkHost>();
            
            Thread thread = new Thread(() => SendIcmpToAll(GetRangeOfAddresses(lowAddress, upAddress)));
            thread.Start();

            

            while (stopwatch.Elapsed <= TimeSpan.FromSeconds(10))
            {
                RawCapture rawCapture = _captureDevice.GetNextPacket();

                if (rawCapture == null)
                {
                    Thread.Sleep(100);
                    continue;
                }

                /**
                 * On regarde quelles machines nous répondent 
                 */
                Packet packet = Packet.ParsePacket(rawCapture.LinkLayerType, rawCapture.Data);

                EthernetPacket ethernetPacket = (EthernetPacket) packet.Extract<EthernetPacket>();
                IPv4Packet ipPacket = (IPv4Packet) packet.Extract<IPv4Packet>();

                if (ipPacket != null)
                {
                    NetworkHost networkHost = new NetworkHost(ethernetPacket.SourceHardwareAddress, ipPacket.SourceAddress);
                    
                    if (!networkHosts.Contains(networkHost))
                        networkHosts.Add(networkHost);
                }
            }

            return networkHosts.ToArray();
        }

        /**
         * Permet d'avoir la liste d'ip à ping des ips dans cette range.
         */
        private IPAddress[] GetRangeOfAddresses(IPAddress low, IPAddress up)
        {
            List<IPAddress> addresses = new List<IPAddress>();
            
            for (int i = low.GetAddressBytes()[3]; i <= up.GetAddressBytes()[3]; i++)
                addresses.Add(
                    new IPAddress(new[] {low.GetAddressBytes()[0], low.GetAddressBytes()[1], low.GetAddressBytes()[2], (byte) i})
                    );

            return addresses.ToArray();
        }

        private void SendIcmpToAll(IPAddress[] addresses)
        {
                foreach (IPAddress address in addresses)
                {
                    Thread thread = new Thread(() =>
                    {
                        Ping ping = new Ping();
                        try
                        {
                            ping.Send(address);
                        }
                        catch (Exception e)
                        {
                            // ignored
                        }
                        finally
                        {
                            ping.Dispose();
                        }
                    });
                    
                    thread.Start();
                }
            
        }
    }
}