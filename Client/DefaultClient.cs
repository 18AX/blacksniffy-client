﻿using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;


namespace Client
{
    public class DefaultClient : Client
    {
        private ClientWebSocket _socket;
        private string url;
        
        public DefaultClient(string url)
        {
            _socket = new ClientWebSocket();
            this.url = url;
        }

        public async Task Connect()
        {
            await _socket.ConnectAsync(new Uri(url), CancellationToken.None);
        }

        public async Task Send(ArraySegment<Byte> bytes)
        {
            await _socket.SendAsync(bytes, WebSocketMessageType.Text, true, CancellationToken.None);
        }

        public async Task<Byte[]> Listening()
        {
            ArraySegment<Byte> buffer = new ArraySegment<byte>(new byte[256]);
            await _socket.ReceiveAsync(buffer, CancellationToken.None);
            return buffer.Array;
        }

        public bool IsConnected()
        {
            return _socket.State != WebSocketState.None;
        }
    }
}