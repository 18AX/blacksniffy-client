﻿using System;
using System.Threading.Tasks;

namespace Client
{
    public interface Client
    {
        Task<Byte[]> Listening();

        Task Send(System.ArraySegment<Byte> bytes);

        bool IsConnected();
    }
}